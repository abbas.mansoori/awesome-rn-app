import React from 'react';

import WebView from 'react-native-webview';
import {Dimensions, StyleSheet, ScrollView} from 'react-native';
import {useNavigation} from '@react-navigation/native';

export const WebViewScreen = () => {
  const navigation = useNavigation();

  return (
    <ScrollView style={styles.view}>
      <WebView
        source={{
          uri: 'https://achmea-nl-2638.app-preview.dev.platform24.se/search',
        }}
        style={styles.webView}
        onMessage={({nativeEvent: {data: jsonData}}) => {
          const {action, ...data} = JSON.parse(jsonData ?? '{}');

          console.log({action, data});

          if (action === 'navigation') {
            navigation.navigate(data?.payload?.page);
          }

          if (action === 'webrtc24.startCall') {
            navigation.navigate('Webrtc24');
          }
        }}
        applicationNameForUserAgent="ExternalP24NativeApp"
        injectedJavaScriptBeforeContentLoaded={`
          (function() {
            window.nativeAppConfig = {};
          })();
        `}
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  view: {flexGrow: 1},
  webView: {
    flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});
