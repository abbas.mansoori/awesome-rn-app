import {useNavigation} from '@react-navigation/native';
import React from 'react';

import {Pressable, StyleSheet, Text, View} from 'react-native';

export const MainScreen = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.view}>
      <Text style={styles.title1}>AwsomeApp</Text>
      <Text style={styles.title2}>Home page</Text>
      <Text style={styles.text}>
        This is a native app view (page) from a foregien app that's not part of
        Platform24.
      </Text>
      <Pressable
        style={styles.button}
        onPress={() => {
          navigation.navigate('WebView');
        }}>
        <Text style={styles.buttonText}>Open P24 Triage</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  view: {display: 'flex', alignItems: 'center', padding: 24},
  title1: {
    textAlign: 'center',
    margin: 12,
    marginBottom: 40,
    fontSize: 24,
    fontWeight: 'bold',
    color: 'blue',
  },
  title2: {textAlign: 'center', margin: 12, fontSize: 20, fontWeight: 'bold'},
  text: {textAlign: 'center', margin: 12, fontSize: 18},
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: 'blue',
    width: 200,
    marginTop: 30,
  },
  buttonText: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },
});
