import React, {useEffect} from 'react';

import {View, StyleSheet} from 'react-native';
import KeepAwake from 'react-native-keep-awake';

import WebRTC24UI from '@platform24/call-ui/src/NativeApp';
import {useNavigation, useRoute} from '@react-navigation/native';

const styles = StyleSheet.create({
  b: {
    backgroundColor: '#ffffff',
    flex: 1,
  },
});

export function WebRTC24Screen() {
  const navigation = useNavigation();
  const route = useRoute();

  const meetingId = route.params.meetingId.toString();
  const token = route.params.token.toString();

  useEffect(() => {
    KeepAwake.activate();
    return () => KeepAwake.deactivate();
  }, []);

  const onClose = React.useCallback(() => {
    navigation.navigate('WebView');
  }, [navigation]);
  return (
    <View style={styles.b}>
      <WebRTC24UI meetingId={meetingId} token={token} onClose={onClose} />
    </View>
  );
}
