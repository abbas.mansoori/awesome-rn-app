import React from 'react';

import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {WebViewScreen} from './WebViewScreen';
import {MainScreen} from './MainScreen';
import {Dimensions, StyleSheet, View} from 'react-native';
import {WebRTC24Screen} from './ios/WebRTC24Screen';

const RootStack = createNativeStackNavigator();

export const RootNavigator = () => {
  return (
    <View style={styles.view}>
      <RootStack.Navigator
        initialRouteName="Main"
        screenOptions={{
          headerShown: false,
        }}>
        <RootStack.Screen
          name="Main"
          navigationKey="Main"
          component={MainScreen}
        />
        <RootStack.Screen name="WebView" component={WebViewScreen} />
        <RootStack.Screen name="Webrtc24" component={WebRTC24Screen} />
      </RootStack.Navigator>
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});
