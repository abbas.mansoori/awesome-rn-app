import React, {useEffect, useState} from 'react';

import WebView from 'react-native-webview';
import {Dimensions, StyleSheet, ScrollView, Button, Text} from 'react-native';
import {useNavigation} from '@react-navigation/native';

export const BackgroundWebView = () => {
  const navigation = useNavigation();

  const [pathname, setPathname] = useState('');
  const [incomingCall, setIncomingCall] = useState(false);
  const [wantsToGoToCall, setWantsToGoToCall] = useState(false);

  return (
    <ScrollView style={styles.view}>
      {incomingCall && (
        <>
          <Text style={{textAlign: 'center'}}>
            There is incoming call from P24
          </Text>
          <Button
            title="Answer P24 call"
            onPress={() => {
              setWantsToGoToCall(true);
              setIncomingCall(false);
            }}
          />
        </>
      )}
      <WebView
        source={{uri: 'http://localhost:8888/history/active'}}
        style={styles.webView}
        containerStyle={
          pathname?.includes('login') || wantsToGoToCall
            ? {}
            : {position: 'absolute', width: 0, height: 0}
        }
        onMessage={({nativeEvent: {data: jsonData}}) => {
          const {action, ...data} = JSON.parse(jsonData ?? '{}');

          if (action === 'incoming_call') {
            setIncomingCall(true);
            console.log('incoming call');
          }

          if (action === 'Route_Change') {
            console.log('pathname c', data.pathname);
            setPathname(data.pathname);
          }

          // if (action === 'webrtc24.startCall') {
          //   navigation.navigate('Webrtc24');
          // }
        }}
        applicationNameForUserAgent="ExternalReactNativeApp"
        injectedJavaScriptBeforeContentLoaded={`
          (function() {
            window.nativeAppConfig = {};
          })();
        `}
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  view: {flexGrow: 1},
  webView: {
    flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});
